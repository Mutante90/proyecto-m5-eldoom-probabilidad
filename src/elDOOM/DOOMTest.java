package elDOOM;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DOOMTest {

	@Test
	void testCalculaMoneda() {
		double res = DOOM.calculaMoneda(4);
		assertEquals(6.25, res);
		//fail("no era el resultado esperado");
	}

	@Test
	void testCalculaDado() {
		double res = DOOM.calculaDado(6);
		assertEquals(0.00214, res);
		//fail("no era el resultado esperado");
	}
}
