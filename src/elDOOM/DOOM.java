package elDOOM;
/**
 * <h1>BUSCAMINAS</h1>
 * 
 * @see <a href="https://gitlab.com/Mutante90/probabilidad-proyecto-m5.git">Proyecto GitLab</a>
 * @author Justo Soria
 * @version 1
 * @since 27/02/2020
 */

import java.util.Scanner;

public class DOOM {
	static Scanner reader = new Scanner(System.in);

		
	public static void main(String[] args) {

		int casos = 0;

		String instrumento = "";
		double resultado = 0;

		casos = reader.nextInt();
		reader.nextLine();

		for (int i = 0; i < casos; i++) {
			instrumento = reader.nextLine().toUpperCase();

			if (instrumento.equals("MONEDA")) {
				int moneda = cuentaCasos(instrumento);
				resultado = calculaMoneda(moneda);
			} else {
				int dado = cuentaCasos(instrumento);
				resultado = calculaDado(dado);
			}
			System.out.println(resultado + "%");
		}

		reader.close();
	}

	/**
	 * en este método le enviamos si el instrumento que usamos para calcular la probabilidad es dado 
	 * o moneda, luego dentro de cada uno de sus 'ifs' contamos el número de veces que ha probado 
	 * surtes el jugador.
	 * 
	 * para contar los intentos, el usuario entrará el resultado, que se guarda en secuencia 
	 * (en caso de moneda, cara o cruz y en caso de dado, del 1 al 6) y acaba con un punto.
	 * 
	 * 
	 * @param cosa: esta variable guarda si es moneda o dado
	 * @return el método devuelve el total de intentos
	 */
	static int cuentaCasos(String cosa) {

		String secuencia = "";
		int intentos = 0;

		if (cosa.equals("MONEDA")) {
			while (!secuencia.equals(".")) {
				secuencia = reader.nextLine().toUpperCase();
				if (secuencia.equals("CARA") || secuencia.equals("CREU")) {
					intentos++;
				}
			}
		} else {
			while (!secuencia.equals(".")) {
				secuencia = reader.nextLine().toUpperCase();
				if (secuencia.equals("1") || secuencia.equals("2") || secuencia.equals("3") || secuencia.equals("4")
						|| secuencia.equals("5") || secuencia.equals("6")) {
					intentos++;
				}
			}
		}
		return intentos;

	}
	
	/**
	 * este método calcula la probabilidad de que el jugador haga la secuencia (con moneda) especificada en
	 * cuentaCasos. la formula usa la probabilidad mínima (la probabilidad de una tirada, en este caso 1/2) 
	 * y lo elevamos al número de intentos que ha hecho el jugador.
	 * 
	 * @param intentos esta variable es el número de intentos que ha devuelto el método cuentaCasos
	 * @return el método devuelve la probabilidad en % de que el jugador haya conseguido esa secuencia
	 */
	static double calculaMoneda(int intentos) {

		double resultado = 0;
		resultado = Math.round((Math.pow(((double) 1 / 2), intentos) * 100) * 1000d) / 1000d;
		
		return resultado;
	}
	
	/**
	 * este método calcula la probabilidad de que el jugador haga la secuencia (con dado) especificada en
	 * cuentaCasos. la formula usa la probabilidad mínima (la probabilidad de una tirada, en este caso 1/6) 
	 * y lo elevamos al número de intentos que ha hecho el jugador.
	 * 
	 * @param intentos esta variable es el número de intentos que ha devuelto el método cuentaCasos
	 * @return el método devuelve la probabilidad en % de que el jugador haya conseguido esa secuencia
	 */
	static double calculaDado(int intentos) {

		double resultado = 0;
		resultado = Math.round((Math.pow(((double) 1 / 6), intentos) * 100) * 100000d) / 100000d;
		
		return resultado;
	}
}
